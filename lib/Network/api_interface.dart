import 'dart:convert';

import 'package:expense_tracker/Models/ResponseModel/login_response.dart';
import 'package:expense_tracker/Network/network_response.dart';
import 'package:expense_tracker/Network/ntlm.dart';
import 'package:expense_tracker/Utils/url_helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:ntlm/ntlm.dart';
import 'package:xml/xml.dart' as xml;

import '../Utils/r_code.dart';

class ApiInterface {
  static String getValueFromXml(xml.XmlDocument parsedXml, tagName) {
    var tagElementValue = parsedXml.findAllElements(tagName);

    if (tagElementValue.isNotEmpty) {
      return (tagElementValue.map((node) => node.value.toString())).first;
    } else {
      return "";
    }
  }

  // Login & Test Connection
  static Future<NetworkResponse> login(String userName, String password) async {
    NTLMClient client = NTLM.initializeNTLM();

    NetworkResponse rs = NetworkResponse();
    var url = UrlHelper.getSOAPUrl("HHTAppFunctions");

    String response = "";
    debugPrint(
        "This is the url helper : ${UrlHelper.getSOAPUrl("HHTAppFunctions")} ${client.username} ${client.password} ${client.domain}");

    var envelope =
        '''<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/">
    <Body>
        <LogInApp xmlns="urn:microsoft-dynamics-schemas/codeunit/HHTAppFunctions">
            <useRef>$userName</useRef>
            <passwordtxt>$password</passwordtxt>
            <assignedLoc></assignedLoc>
        </LogInApp>
    </Body>
</Envelope>''';
    debugPrint(envelope);
    await client.post(
      Uri.parse(url),
      headers: {
        "Content-Type": "text/xml",
        "Accept-Charset": "utf-8",
        "SOAPAction": "urn:microsoft-dynamics-schemas/codeunit/HHTAppFunctions",
      },
      body: envelope,
      encoding: Encoding.getByName("UTF-8"),
    ).then((res) {
      debugPrint("This is the response ${res.body}");
      var rawXmlResponse = res.body;

      if (res.body.isEmpty) {
        rs.status = res.statusCode;
      } else {
        xml.XmlDocument parsedXml = xml.XmlDocument.parse(rawXmlResponse);

        if (res.statusCode == RCode.SUCCESS) {
          LoginResponse loginResponse = LoginResponse();
          loginResponse.returnValue =
              getValueFromXml(parsedXml, "return_value");
          loginResponse.assignedLocation =
              getValueFromXml(parsedXml, "assignedLoc");
          loginResponse.uID = getValueFromXml(parsedXml, "uID");

          rs.loginResponse = loginResponse;
          rs.responseBody = response;
          rs.status = res.statusCode;
        } else {
          response = getValueFromXml(parsedXml, "faultstring");
          rs.responseBody = response;
          rs.status = res.statusCode;
        }
      }
    });

    return rs;
  }
}
