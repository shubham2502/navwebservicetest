import 'package:expense_tracker/Models/ResponseModel/login_response.dart';

class NetworkResponse {
  String? responseBody;
  int? status;

  LoginResponse? loginResponse;

  NetworkResponse({
    this.responseBody,
    this.status,
    this.loginResponse,
  });
}
