import 'package:expense_tracker/Utils/constants.dart';
import 'package:expense_tracker/Utils/sp_utils.dart';
import 'package:ntlm/ntlm.dart';

class NTLM {
  static NTLMClient initializeNTLM() {
    NTLMClient client = NTLMClient(
      domain: SpUtil.getString(Constants.NTLMDOMAIN),
      workstation: "",
      username: SpUtil.getString(Constants.NTLMUSERNAME),
      password: SpUtil.getString(Constants.NTLMPASSWORD),
    );
    return client;
  }
}
