
import 'package:expense_tracker/Utils/constants.dart';
import 'package:expense_tracker/Utils/sp_utils.dart';

class UrlHelper {
  static String getSOAPUrl(String method) {
    String server = SpUtil.getString(Constants.SERVER);
    String service = SpUtil.getString(Constants.SERVICE);
    String company = SpUtil.getString(Constants.COMPANY);
    String web = SpUtil.getString(Constants.WEB);
    String port = SpUtil.getString(Constants.SOAP_PORT);

    String url = "http://$server:$port/$service/$web/$company/Codeunit/$method";
    return Uri.encodeFull(url);
  }

  static String getPageSOAPUrl(String method) {
    String server = SpUtil.getString(Constants.SERVER);
    String service = SpUtil.getString(Constants.SERVICE);
    String company = SpUtil.getString(Constants.COMPANY);
    String web = SpUtil.getString(Constants.WEB);
    String port = SpUtil.getString(Constants.SOAP_PORT);

    String url = "http://$server:$port/$service/$web/$company/Page/$method";
    return Uri.encodeFull(url);
  }

  static String getODATAUrl(String method, {String? filter}) {
    String server = SpUtil.getString(Constants.SERVER);
    String service = SpUtil.getString(Constants.SERVICE);
    String company = SpUtil.getString(Constants.COMPANY);
    String port = SpUtil.getString(Constants.ODATA_PORT);

    String url =
        "http://$server:$port/$service/ODataV4/Company('$company')/$method?\$format=json";

    if (filter != null) {
      url = url + filter;
    }

    return Uri.encodeFull(url);
  }
}
