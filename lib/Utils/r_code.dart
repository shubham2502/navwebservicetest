class RCode {
  static const int SUCCESS = 200;
  static const int CREATED = 201;
  static const int UNAUTHORIZED = 401;
  static const int EXPIRED = 410;
  static const int INTERNAL_SERVER_ERROR = 500;
}
