import 'package:expense_tracker/Network/network_connection.dart';
import 'package:flutter/material.dart';

class UiUtils {
  static void showSnackBar(BuildContext context, String? message,
      {SnackBarBehavior? snackBarBehavior}) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        behavior: snackBarBehavior,
        duration: const Duration(seconds: 3),
        content: Text(message!),
      ),
    );
  }

  static void showSnackBarWithProgressBar(
      BuildContext context, String message) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Row(
        children: [
          const CircularProgressIndicator(),
          const SizedBox(width: 20),
          Text(message)
        ],
      ),
    ));
  }

  static void showInternetSnackBar(BuildContext context, String message, Function func, {SnackBarBehavior? snackBarBehavior,}) {
    NetworkConnectionCheck networkConnectionCheck = NetworkConnectionCheck();
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        behavior: snackBarBehavior,
        duration: const Duration(minutes: 5),
        content: Text(message),
        action: SnackBarAction(
          label: "Retry",
          onPressed: () {
            ScaffoldMessenger.of(context).hideCurrentSnackBar();
            networkConnectionCheck.checkInternet(func);
          },
        ),
      ),
    );
  }
}
