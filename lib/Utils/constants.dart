class Constants {
  static const String CONFIGURED = 'is_configured';
  static const String SERVER = "server";
  static const String SERVICE = "service";
  static const String COMPANY = "company";
  static const String ODATA_PORT = "odata_port";
  static const String PORT = "port";
  static const String WEB = "web";
  static const String SOAP_PORT = "soap_port";
  static const String NTLMDOMAIN = "ntlmDomain";
  static const String NTLMUSERNAME = "ntlmUsername";
  static const String NTLMPASSWORD = "ntlmPassword";
  static const String TESTLOGINUSER = "testLoginUser";
  static const String TESTLOGINPASSWORD = "testLoginPassword";

  static const String LAST_LOGGED_USERNAME = "last_logged_username";
  static const String ASSIGNED_LOCATION = "assigned_loc";

}
