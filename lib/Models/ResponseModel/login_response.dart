class LoginResponse {
  String? returnValue;
  String? assignedLocation;
  String? uID;

  LoginResponse({this.returnValue, this.assignedLocation, this.uID});
}
