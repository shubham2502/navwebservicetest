import 'package:expense_tracker/Screens/login.dart';
import 'package:expense_tracker/Utils/sp_utils.dart';
import 'package:flutter/material.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await SpUtil.getInstance(); // initiate SPUtil

  runApp(
    const MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "db test",
      home: LoginScreen(),
    )
  );
}
