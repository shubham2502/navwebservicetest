import 'package:expense_tracker/Models/ResponseModel/login_response.dart';
import 'package:expense_tracker/Network/api_interface.dart';
import 'package:expense_tracker/Screens/config.dart';
import 'package:expense_tracker/Screens/dashboard.dart';
import 'package:expense_tracker/Utils/constants.dart';
import 'package:expense_tracker/Utils/r_code.dart';
import 'package:expense_tracker/Utils/sp_utils.dart';
import 'package:expense_tracker/Utils/ui_utils.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  LoginScreenState createState() => LoginScreenState();
}

class LoginScreenState extends State<LoginScreen> {
  bool isPassword = true, isProgressBarShown = false;

  // TextEditingController usernameController =
  //     TextEditingController(text: "EC2AMAZ-C7UK3OR\\ADMINISTRATOR");
  // TextEditingController passwordController =
  //     TextEditingController(text: "12345");

  TextEditingController usernameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  bool isBarcodeManual = false;

  String companyName = "";

  @override
  void initState() {
    super.initState();

    getCompanyName();
  }

  void getCompanyName() {
    if (SpUtil.getString(Constants.COMPANY).isNotEmpty) {
      setState(() {
        companyName = SpUtil.getString(Constants.COMPANY);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: ModalProgressHUD(
        inAsyncCall: isProgressBarShown,
        dismissible: true,
        progressIndicator: const CircularProgressIndicator(
          backgroundColor: Colors.blue,
        ),
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                height: MediaQuery.of(context).size.height - 70,
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      if (companyName.isNotEmpty)
                        Text(
                          companyName,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 40,
                            color: Theme.of(context).primaryColor,
                          ),
                        ),
                      Text(
                        'Sales Picking',
                        style: TextStyle(
                          fontSize: 20,
                          color: Theme.of(context).primaryColor,
                        ),
                      ),
                      const SizedBox(
                        height: 50,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 25),
                        child: Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.symmetric(vertical: 5),
                              child: Container(
                                decoration: BoxDecoration(
                                  color: Colors.grey[200],
                                  borderRadius: BorderRadius.circular(20.0),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      left: 15, right: 15),
                                  child: TextFormField(
                                    autofocus: !isBarcodeManual,
                                    controller: usernameController,
                                    textInputAction: TextInputAction.next,
                                    decoration: const InputDecoration(
                                      border: InputBorder.none,
                                      labelText: 'Username',
                                      labelStyle: TextStyle(
                                        color: Colors.black,
                                        fontSize: 14,
                                      ),
                                    ),
                                    autovalidateMode:
                                    AutovalidateMode.onUserInteraction,
                                    validator: (username) {
                                      if (username!.isEmpty) {
                                        return "Username can't be empty!";
                                      }
                                      return null;
                                    },
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 5, bottom: 5),
                              child: Container(
                                decoration: BoxDecoration(
                                  color: Colors.grey[200],
                                  borderRadius: BorderRadius.circular(20.0),
                                ),
                                child: Row(
                                  children: [
                                    Expanded(
                                      flex: 6,
                                      child: Padding(
                                        padding: const EdgeInsets.only(
                                          left: 15,
                                          right: 15,
                                        ),
                                        child: TextFormField(
                                          autofocus: !isBarcodeManual,
                                          controller: passwordController,
                                          obscureText: isPassword,
                                          decoration: const InputDecoration(
                                            border: InputBorder.none,
                                            labelText: 'Password',
                                            labelStyle: TextStyle(
                                              fontFamily: 'Montserrat',
                                              color: Colors.black,
                                              fontSize: 14,
                                            ),
                                          ),
                                          onFieldSubmitted: ((value) {
                                            performLogin();
                                          }),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(vertical: 5),
                              child: SizedBox(
                                width: double.infinity,
                                child: ElevatedButton(
                                  onPressed: () {
                                    performLogin();
                                  },
                                  style: ButtonStyle(
                                    shape: MaterialStateProperty.all<
                                        RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                        borderRadius:
                                        BorderRadius.circular(18.0),
                                        side:
                                        const BorderSide(color: Colors.red),
                                      ),
                                    ),
                                  ),
                                  child: const Padding(
                                    padding: EdgeInsets.all(13.0),
                                    child: Text(
                                      'LOGIN',
                                      style: TextStyle(
                                        fontSize: 18,
                                        // color: Colors.green[700],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                mainAxisAlignment:
                                MainAxisAlignment.spaceBetween,
                                children: [
                                  InkWell(
                                    onTap: () {

                                    },
                                    child: Text(
                                      "Master Sync",
                                      style: TextStyle(
                                        fontWeight: FontWeight.w500,
                                        fontSize: 18,
                                        color: Colors.red[300],
                                      ),
                                    ),
                                  ),
                                  InkWell(
                                    onTap: () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) =>
                                          const ConfigScreen(),
                                        ),
                                      ).then((value) => getCompanyName());
                                    },
                                    child: Text(
                                      "Configure",
                                      style: TextStyle(
                                        fontWeight: FontWeight.w500,
                                        fontSize: 18,
                                        color: Colors.red[300],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void performLogin() async {
    if (SpUtil.getBool(Constants.CONFIGURED)) {
      if (usernameController.text.isNotEmpty) {
        showProgressBar();
        ApiInterface.login(
          usernameController.text,
          passwordController.text,
        ).then((res) {
          hideProgressBar();

          if (res.status == RCode.SUCCESS) {
            LoginResponse loginResponse = LoginResponse();
            loginResponse = res.loginResponse!;

            SpUtil.putString(Constants.LAST_LOGGED_USERNAME,
                loginResponse.uID);
            SpUtil.putString(Constants.ASSIGNED_LOCATION,
                loginResponse.assignedLocation);

            Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => const Dashboard(),
              ), (route) => false,
            );
          } else {
            UiUtils.showSnackBar(context, res.responseBody);
          }
        }).catchError((err) {
          hideProgressBar();
          UiUtils.showSnackBar(context, err.toString());
        });
      } else {
        UiUtils.showSnackBar(
            context, "Username can't be left empty!");
      }
    } else {
      UiUtils.showSnackBar(context, 'Please configure first!');
    }
  }

  void showProgressBar() {
    setState(() {
      isProgressBarShown = true;
    });
  }

  void hideProgressBar() {
    setState(() {
      isProgressBarShown = false;
    });
  }
}
