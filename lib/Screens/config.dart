import 'package:expense_tracker/Network/api_interface.dart';
import 'package:expense_tracker/Network/network_connection.dart';
import 'package:expense_tracker/Utils/constants.dart';
import 'package:expense_tracker/Utils/r_code.dart';
import 'package:expense_tracker/Utils/sp_utils.dart';
import 'package:expense_tracker/Utils/ui_utils.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';
import 'package:ntlm/ntlm.dart';

class ConfigScreen extends StatefulWidget {
  const ConfigScreen({Key? key}) : super(key: key);

  @override
  State<ConfigScreen> createState() => _ConfigScreenState();
}

class _ConfigScreenState extends State<ConfigScreen> {

  final _formKey = GlobalKey<FormState>();
  bool isProgressBarShown = false;
  NetworkConnectionCheck networkConnectionCheck = NetworkConnectionCheck();
  NTLMClient? client;

  TextEditingController serverController = TextEditingController();
  TextEditingController companyController = TextEditingController();
  TextEditingController serviceController = TextEditingController();
  TextEditingController webController = TextEditingController();
  TextEditingController odataController = TextEditingController();
  TextEditingController soapController = TextEditingController();
  TextEditingController domainController = TextEditingController();
  TextEditingController usernameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController testUserNameController = TextEditingController();
  TextEditingController testPasswordController = TextEditingController();

  @override
  void initState() {
    getServerDetailsIfAvailable();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Configuration Manager"),
      ),
      body: ModalProgressHUD(
        inAsyncCall: isProgressBarShown,
        child: SingleChildScrollView(
          child: Container(
            margin: const EdgeInsets.all(10),
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  TextFormField(
                    controller: serverController,
                    textInputAction: TextInputAction.next,
                    decoration: const InputDecoration(
                      labelText: 'SERVER',
                      labelStyle: TextStyle(
                        fontFamily: 'Montserrat',
                        fontWeight: FontWeight.w600,
                        fontSize: 14,
                        color: Colors.grey,
                      ),
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(
                          color: Color(0xFFd1b8b8),
                        ),
                      ),
                    ),
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    validator: (text) {
                      if (text == null || text.isEmpty) {
                        return 'This field is required!';
                      }
                      return null;
                    },
                  ),
                  const SizedBox(height: 10.0),
                  TextFormField(
                    controller: serviceController,
                    textInputAction: TextInputAction.next,
                    decoration: const InputDecoration(
                        labelText: 'SERVICE',
                        labelStyle: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 14,
                            color: Colors.grey),
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Color(0xFFd1b8b8)))),
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    validator: (text) {
                      if (text == null || text.isEmpty) {
                        return 'This field is required!';
                      }
                      return null;
                    },
                  ),
                  const SizedBox(height: 10.0),
                  TextFormField(
                    controller: companyController,
                    textInputAction: TextInputAction.next,
                    decoration: const InputDecoration(
                        labelText: 'COMPANY',
                        labelStyle: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 14,
                            color: Colors.grey),
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Color(0xFFd1b8b8)))),
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    validator: (text) {
                      if (text == null || text.isEmpty) {
                        return 'This field is required!';
                      }
                      return null;
                    },
                  ),
                  const SizedBox(height: 10.0),
                  TextFormField(
                    controller: webController,
                    textInputAction: TextInputAction.next,
                    decoration: const InputDecoration(
                        labelText: 'WEB',
                        labelStyle: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 14,
                            color: Colors.grey),
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Color(0xFFd1b8b8)))),
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    validator: (text) {
                      if (text == null || text.isEmpty) {
                        return 'This field is required!';
                      }
                      return null;
                    },
                  ),
                  const SizedBox(height: 10.0),
                  TextFormField(
                    controller: odataController,
                    textInputAction: TextInputAction.next,
                    decoration: const InputDecoration(
                        labelText: 'ODATA PORT',
                        labelStyle: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 14,
                            color: Colors.grey),
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Color(0xFFd1b8b8)))),
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    keyboardType: TextInputType.number,
                    validator: (text) {
                      if (text == null || text.isEmpty) {
                        return 'This field is required!';
                      }
                      return null;
                    },
                  ),
                  const SizedBox(height: 10.0),
                  TextFormField(
                    controller: soapController,
                    textInputAction: TextInputAction.next,
                    decoration: const InputDecoration(
                        labelText: 'SOAP PORT',
                        labelStyle: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 14,
                            color: Colors.grey),
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Color(0xFFd1b8b8)))),
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    keyboardType: TextInputType.number,
                    validator: (text) {
                      if (text == null || text.isEmpty) {
                        return 'This field is required!';
                      }
                      return null;
                    },
                  ),
                  const SizedBox(height: 10.0),
                  TextFormField(
                    controller: domainController,
                    textInputAction: TextInputAction.next,
                    decoration: const InputDecoration(
                        labelText: 'DOMAIN',
                        labelStyle: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 14,
                            color: Colors.grey),
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Color(0xFFd1b8b8)))),
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    validator: (text) {
                      if (text == null || text.isEmpty) {
                        return 'This field is required!';
                      }
                      return null;
                    },
                  ),
                  const SizedBox(height: 10.0),
                  TextFormField(
                    controller: usernameController,
                    textInputAction: TextInputAction.next,
                    decoration: const InputDecoration(
                        labelText: 'USERNAME',
                        labelStyle: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 14,
                            color: Colors.grey),
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Color(0xFFd1b8b8)))),
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    validator: (text) {
                      if (text == null || text.isEmpty) {
                        return 'This field is required!';
                      }
                      return null;
                    },
                  ),
                  const SizedBox(height: 10.0),
                  TextFormField(
                    controller: passwordController,
                    obscureText: true,
                    textInputAction: TextInputAction.next,
                    decoration: const InputDecoration(
                        labelText: 'PASSWORD',
                        labelStyle: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 14,
                            color: Colors.grey),
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Color(0xFFd1b8b8)))),
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    validator: (text) {
                      if (text == null || text.isEmpty) {
                        return 'This field is required!';
                      }
                      return null;
                    },
                  ),
                  const SizedBox(height: 10.0),
                  TextFormField(
                    controller: testUserNameController,
                    textInputAction: TextInputAction.next,
                    decoration: const InputDecoration(
                        labelText: 'TEST USERNAME',
                        labelStyle: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 14,
                            color: Colors.grey),
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Color(0xFFd1b8b8)))),
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    validator: (text) {
                      if (text == null || text.isEmpty) {
                        return 'This field is required!';
                      }
                      return null;
                    },
                  ),
                  const SizedBox(height: 10.0),
                  TextFormField(
                    controller: testPasswordController,
                    obscureText: true,
                    decoration: const InputDecoration(
                        labelText: 'TEST PASSWORD',
                        labelStyle: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 14,
                            color: Colors.grey),
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Color(0xFFd1b8b8)))),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  const SizedBox(height: 42.0),
                  SizedBox(
                    height: 42.0,
                    child: Material(
                      borderRadius: BorderRadius.circular(20.0),
                      color: Theme.of(context).primaryColor,
                      elevation: 1.0,
                      child: InkWell(
                        onTap: () {
                          networkConnectionCheck
                              .checkInternet(checkInternetForTestConnection);
                        },
                        child: Ink(
                          child: const Center(
                            child: Text(
                              'TEST CONNECTION',
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  checkInternetForTestConnection(bool isInternetConnected) {
    //check the status of the form fields and validate
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();

      //proceed with the process after the form is validated.
      if (isInternetConnected) {
        //first save the credentials into prefs, then proceed with the processes.
        saveNtlmCredentialsToPrefs().whenComplete(() {
          testConnection();
        });
      } else {
        UiUtils.showInternetSnackBar(
            context, "No Internet Connection", checkInternetForTestConnection);
      }
    } else {
      UiUtils.showSnackBar(context, "Validation failed for one or more fields");
    }
  }

  void testConnection() async {
    showProgressBar();

    FocusScope.of(context).requestFocus(FocusNode());

    await ApiInterface.login(
      testUserNameController.text,
      testPasswordController.text,
    ).then((rs) {
      if (rs.status == RCode.SUCCESS) {
        hideProgressBar();
        SpUtil.putBool(Constants.CONFIGURED, true);
        UiUtils.showSnackBar(context, "Test Connection Successful!!");
        Navigator.pop(context);
      } else if (rs.status == RCode.UNAUTHORIZED) {
        hideProgressBar();
        UiUtils.showSnackBar(context, "The username or password is incorrect");
      } else {
        hideProgressBar();
        UiUtils.showSnackBar(context, rs.responseBody);
      }
    }).catchError((e) {
      hideProgressBar();
      UiUtils.showSnackBar(context, e.toString());
    });
  }

  Future<void> saveNtlmCredentialsToPrefs() async {
    String server = serverController.text;
    String service = serviceController.text;
    String company = companyController.text;
    String web = webController.text;
    String odata = odataController.text;
    String soap = soapController.text;
    String domain = domainController.text;
    String username = usernameController.text;
    String password = passwordController.text;
    String testLogin = testUserNameController.text;
    String testPassword = testPasswordController.text;

    debugPrint("server : $server");
    debugPrint("service : $service");
    debugPrint("company : $company");
    debugPrint("web : $web");
    debugPrint("odata : $odata");
    debugPrint("soap : $soap");
    debugPrint("domain : $domain");
    debugPrint("username : $username");
    debugPrint("password : $password");
    debugPrint("testLogin : $testLogin");
    debugPrint("testPassword : $testPassword");

    SpUtil.putString(Constants.SERVER, server);
    SpUtil.putString(Constants.SERVICE, service);
    SpUtil.putString(Constants.COMPANY, company);
    SpUtil.putString(Constants.WEB, web);
    SpUtil.putString(Constants.ODATA_PORT, odata);
    SpUtil.putString(Constants.SOAP_PORT, soap);
    SpUtil.putString(Constants.NTLMDOMAIN, domain);
    SpUtil.putString(Constants.NTLMUSERNAME, username);
    SpUtil.putString(Constants.NTLMPASSWORD, password);
    SpUtil.putString(Constants.TESTLOGINUSER, testLogin);
    SpUtil.putString(Constants.TESTLOGINPASSWORD, testPassword);
  }

  void getServerDetailsIfAvailable() async {
    if (SpUtil.getString(Constants.SERVER).isNotEmpty) {
      setState(() {
        serverController.text = SpUtil.getString(Constants.SERVER);
        serviceController.text = SpUtil.getString(Constants.SERVICE);
        webController.text = SpUtil.getString(Constants.WEB);
        companyController.text = SpUtil.getString(Constants.COMPANY);
        odataController.text = SpUtil.getString(Constants.ODATA_PORT);
        soapController.text = SpUtil.getString(Constants.SOAP_PORT);
        domainController.text = SpUtil.getString(Constants.NTLMDOMAIN);
        usernameController.text = SpUtil.getString(Constants.NTLMUSERNAME);
        passwordController.text = SpUtil.getString(Constants.NTLMPASSWORD);
        testUserNameController.text = SpUtil.getString(Constants.TESTLOGINUSER);
        testPasswordController.text =
            SpUtil.getString(Constants.TESTLOGINPASSWORD);
      });
    }
  }

  void showProgressBar() {
    setState(() {
      isProgressBarShown = true;
    });
  }

  void hideProgressBar() {
    setState(() {
      isProgressBarShown = false;
    });
  }
}
